/*
 * @Author: JHank
 * @Date: 2022-09-09 20:20:10
 * @LastEditors: JHank
 * @LastEditTime: 2022-09-11 22:04:55
 * @FilePath: \DataStructureTemplate\graph\adjacencyList.c
 * @Description:邻接表表示的图结构源文件
 * hank.jhk@outlook.com
 * Copyright (c) 2022 by JHank, All Rights Reserved.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "adjacencyList.h"


/**************************************************/
/******************边链表操作函数*******************/
/**************************************************/


/**
 * @description:边节点创建函数
 * @param {Vertex} adjV：头节点编号
 * @param {WeightType} weight：边权重
 * @return {*}创建的边节点
 */
pEdgeNode AdjGraphCreateENode(Vertex adjV,WeightType weight)
{
    pEdgeNode pnode;
    pnode = (pEdgeNode)malloc(sizeof(EdgeNode));
    pnode->adjV = adjV;
    pnode->weight = weight;
    pnode->next = NULL;
    return pnode;
}


/**
 * @description:边节点尾插函数
 * @param {pEdgeNode*} ppheader：边链表头节点二级指针
 * @param {pEdgeNode} pnode：要插入的边节点指针
 * @return {*} void
 */
void AdjGraphInsertLastENode(pEdgeNode* ppheader, pEdgeNode pnode)
{
    pEdgeNode* pplink = ppheader;
    while ((*pplink) != NULL) {
        pplink = &(*pplink)->next;
    }
    *pplink = pnode;
    pnode->next = NULL;
}


/**
 * @description:边节点首插函数
 * @param {pEdgeNode*} ppheader：边链表头节点二级指针
 * @param {pEdgeNode} pnode：要插入的节点指针
 * @return {*} void
 */
void AdjGraphInsertHeadENode(pEdgeNode* ppheader, pEdgeNode pnode)
{
    pnode->next = (*ppheader);
    *ppheader = pnode;
}


/**
 * @description:边链表删除头节点函数
 * @param {pEdgeNode*} ppheader：头节点二级指针
 * @return {*} true： 删除成功
 *             false：删除失败(没有节点)
 */
bool AdjGraphDeleteHeadENode(pEdgeNode* ppheader)
{
    if (*ppheader != NULL) {
        pEdgeNode ppTemp = *ppheader;
        *ppheader = (*ppheader)->next;  //BUG?
        free(ppTemp);
        return true;
    }
    else {
        return false;
    }
}


/**
 * @description:边链表尾节点删除函数
 * @param {pEdgeNode*} ppheader：边链表头节点二级指针
 * @return {*} true： 删除成功
 *             false：删除失败(没有节点)
 */
bool AdjGraphDeleteLastENode(pEdgeNode* ppheader)
{
    if (*ppheader != NULL) {
        pEdgeNode* pplink = ppheader;
        while ((*pplink)->next != NULL) {
            pplink = &(*pplink)->next;
        }
        free(*pplink);  //释放内存
        *pplink = NULL;
        return true;
    }
    else {
        return false;
    }
}



/**
 * @description:边链表删除选定节点函数
 * @param {pEdgeNode*} ppheader：边链表头节点二级指针
 * @param {Vertex} adjV：要删除的节点的节点编号
 * @return {*} true： 删除成功
 *             false：删除失败(没有节点)
 */
bool AdjGraphDeleteENode(pEdgeNode* ppheader, Vertex adjV)
{
    pEdgeNode* pplink = ppheader;
    pEdgeNode temp;

    while((*pplink) != NULL){
        if((*pplink)->adjV == adjV){
            temp = (*pplink)->next;
            free(*pplink);
            *pplink = temp;
            return true;    //节点删除成功
        }
        pplink = &(*pplink)->next;  //下一节点
    }
    return false;   //节点删除失败，没有这个节点
}



/**
 * @description:边链表清空函数
 * @param {pEdgeNode*} ppheader：边链表头节点二级指针
 * @return {*} void
 */
void AdjGraphEmptyENode(pEdgeNode* ppheader)
{
    pEdgeNode* pplink = ppheader;
    pEdgeNode temp;
    while ((*pplink) != NULL) {
        temp = (*pplink);
        *pplink = (*pplink)->next;
        free(temp);
    }
}


/**
 * @description:边链表节点搜索函数
 * @param {pEdgeNode*} ppheader：边链表二级指针
 * @param {Vertex} adjV：需要的边节点代表的图节点编号
 * @param {WeightType*} weightBack：需要的边节点的权重返回指针
 * @return {*}  true： 检索成功
 *              false：检索失败(没有此节点)
 */
bool AdjGraphFindENode(pEdgeNode* ppheader, Vertex adjV, WeightType* weightBack)
{
    pEdgeNode* pplink = ppheader;

    while ((*pplink) != NULL) {
        if((*pplink)->adjV == adjV){
            *weightBack = (*pplink)->weight;
            return true;
        }
        pplink = &(*pplink)->next;
    }
    return false;
}


/**
 * @description:边链表长度读取函数
 * @param {pEdgeNode*} ppheader：边链表头节点二级指针
 * @return {*}边链表长度(int)
 */
int AdjGraphELength(pEdgeNode* ppheader)
{
    int cnt = 0;
    pEdgeNode* pplink = ppheader;

    while ((*pplink) != NULL) {
        cnt++;
        pplink = &(*pplink)->next;
    }
    return cnt;
}



/**************************************************/
/*****************图节点链表操作函数 ****************/
/**************************************************/


/******************************************************************/
/*头节点链表的所有删除操作都应该考虑到邻接表链表的问题，不能直接将头节点
 链表中的任意节点直接删除，在删除前应先将头节点中的邻接表处理好，以及其他
 节点中与要删除节点有边相连的邻接节点也要考虑*/
/******************************************************************/


/**
 * @description:图初始化
 * @param {pVertexNode} pheader：头节点指针
 * @return {*}
 */
void AdjGraphInit(pVertexNode pheader)
{
    pheader = NULL;
}


/**
 * @description:图节点创建函数
 * @param {Vertex} adjV：图节点编号
 * @param {pAdjDataType} pvertexData：当前图节点数据结构指针
 * @param {int} numVEdges：当前图节点所链接的边数
 * @param {Vertex} adjVE：当前图节点的邻接边链表中顺序图节点编号
 * @param {WeightType} weightE：当前图节点的邻接边链表中顺序边权重
 * @return {*}
 */
pVertexNode AdjGraphCreateVNode(Vertex adjV, pAdjDataType pvertexData, int numVEdges,Vertex adjVE[],WeightType weightE[])
{
    pVertexNode pnode;
    pnode = (pVertexNode)malloc(sizeof(VertexNode));
    pnode->adjV = adjV;
    pnode->numVEdges = numVEdges;
    pnode->pvertexData = pvertexData;   //数据初始化,以上
    pnode->next = NULL;         //下一节点初始为null

    pnode->pfirstEdge = NULL;   //边链表头指针null
    for(int i=0;i++;i<numVEdges){
        AdjGraphInsertLastENode(&(pnode->pfirstEdge),AdjGraphCreateENode(adjVE[i],weightE[i]));
    }

    return pnode;
}


/**
 * @description:图节点链表插入节点函数(无空头节点，从0开始计算第一个节点)
 * @param {pVertexNode*} ppheader:头节点链表头二级指针;
 * @param {pVertexNode} pnode:待插入节点;
 * @param {int} addr:待插入下标(从0开始)
 * @return {*}  true:插入成功
 *              false:插入失败
 */
bool AdjGraphInsertVNode(pVertexNode* ppheader, pVertexNode pnode, int addr)
{
    pVertexNode* pplink = ppheader;
    pVertexNode pcurrent = *ppheader;
    int cnt = 0;  // current现在的节点下标，从0开始

    while (pcurrent != NULL && cnt < addr) {
        pplink = &pcurrent->next;
        pcurrent = (*pplink);
        cnt++;
    }
    if (pcurrent != NULL) {
        (*pplink) = pnode;
        pnode->next = pcurrent;
        return true;
    }
    else {
        return false;  //邻接表长度不够
    }
}



/**
 * @description:图节点链表尾插函数
 * @param {pVertexNode*} ppheader：邻接表二级头指针；
 * @param {pVertexNode} pnode：待插入的节点
 * @return {*}void
 */
void AdjGraphInsertLastVNode(pVertexNode* ppheader, pVertexNode pnode)
{
    pVertexNode* pplink = ppheader;
    while ((*pplink) != NULL) {
        pplink = &(*pplink)->next;
    }
    *pplink = pnode;
    pnode->next = NULL;
}



/**
 * @description:图节点链表头插函数
 * @param {pVertexNode*} ppheader：邻接表二级头指针；
 * @param {pVertexNode} pnode：待插入的节点
 * @return {*} void
 */
void AdjGraphInsertHeadVNode(pVertexNode* ppheader, pVertexNode pnode)
{
    pnode->next = (*ppheader);
    *ppheader = pnode;
}


/**
 * @description:图节点链表头删除函数
 * @param {pVertexNode*} ppheader：邻接表二级头指针
 * @return {*} true： 删除成功
 *             false：删除失败(没有节点)
 */
bool AdjGraphDeleteHeadVNode(pVertexNode* ppheader)
{
    pVertexNode ppTemp;

    if (*ppheader != NULL) {
        AdjGraphEmptyENode(&((*ppheader)->pfirstEdge)); //待删除图节点的边链表释放
        ppTemp = *ppheader;
        *ppheader = (*ppheader)->next;
        free(ppTemp);
        return true;
    }
    else {
        return false;
    }
}


/**
 * @description:图节点链表尾删除函数
 * @param {pVertexNode*} ppheader：邻接表二级头指针
 * @return {*}  true：删除成功
 *              false：删除失败(没有节点)
 */
bool AdjGraphDeleteLastVNode(pVertexNode* ppheader)
{
    pVertexNode* pplink;

    if (*ppheader != NULL) {
        pplink = ppheader;
        while ((*pplink)->next != NULL) {
            pplink = &(*pplink)->next;
        }
        AdjGraphEmptyENode(&((*pplink)->pfirstEdge)); //待删除图节点的边链表释放
        free(*pplink);
        *pplink = NULL;
        return true;
    }
    else {
        return false;
    }
}


/**
 * @description:图节点链表节点删除函数
 * @param {pVertexNode*} ppheader：邻接表二级头指针
 * @param {Vertex} adjV：待删除图节点的节点编号
 * @return {*} true：删除成功
 *             false：删除失败(没有该节点)
 */
bool AdjGraphDeleteVNode(pVertexNode* ppheader, Vertex adjV)
{
    pVertexNode* pplink = ppheader;
    pVertexNode temp;

    while ((*pplink) != NULL) {
        if((*pplink)->adjV == adjV){    //正中下怀！
            AdjGraphEmptyENode(&((*pplink)->pfirstEdge)); //待删除图节点的边链表释放
            temp = (*pplink)->next;
            free(*pplink);
            *pplink = temp;
            return true;
        }
        pplink = &(*pplink)->next;
    }
    return false;
}


/**
 * @description:图节点链表清空函数
 * @param {pVertexNode*} ppheader:邻接表二级头指针
 * @return {*} void
 */
void AdjGraphEmptyVNode(pVertexNode* ppheader)
{
    pVertexNode* pplink = ppheader;
    pVertexNode temp;
    while ((*pplink) != NULL) {
        AdjGraphEmptyENode(&((*pplink)->pfirstEdge)); //待删除图节点的边链表释放
        temp = (*pplink);
        *pplink = (*pplink)->next;
        free(temp);
    }
}


/**
 * @description: 图节点链表搜索函数
 * @param : ppheader：单邻接表二级头指针
 *          addr：查找结点下标(从0开始)
 *          a:查找结点数据返回指针
 * @return: true：查找成功
 *          false：查找失败(没有该节点)
 */
bool AdjGraphFindVNode(pVertexNode* ppheader, Vertex adjV, pAdjDataType pvertexData)
{
    pVertexNode* pplink = ppheader;

    while ((*pplink) != NULL) {
        if((*pplink)->adjV == adjV){
            pvertexData = (*pplink)->pvertexData;
            return true;
        }
        pplink = &(*pplink)->next;
    }
    return false;
}


/**
 * @description: 图节点链表长度读取函数
 * @param : ppheader：邻接表二级头指针
 * @return: 邻接表长度(真实长度)
 */
int AdjGraphVLength(pVertexNode* ppheader)
{
    int cnt = 0;
    pVertexNode* pplink = ppheader;

    while ((*pplink) != NULL) {
        cnt++;
        pplink = &(*pplink)->next;
    }
    return cnt;
}


/**
 * @description: 图节点链表逆序函数
 * @param : 处理前邻接表头指针
 * @return: 处理后邻接表头指针
 */
pVertexNode AdjGraphVReverse(pVertexNode oldHeader)
{
    pVertexNode pOldList, pNewList, ptemp;
    pOldList = oldHeader;
    pNewList = NULL;

    while (pOldList != NULL) {
        ptemp = pOldList->next;     //处理位置原始后移预备
        pOldList->next = pNewList;  //邻接表指向交换
        pNewList = pOldList;        //邻接表指向交换
        pOldList = ptemp;           //处理位置原始后移
    }
    return pNewList;  //设置新邻接表头
}



