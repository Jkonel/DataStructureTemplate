/***
 * @Author: JHank
 * @Date: 2022-09-09 20:20:33
 * @LastEditors: JHank
 * @LastEditTime: 2022-09-09 20:20:35
 * @FilePath: \DataStructureTemplate\graph\adjacencyList.h
 * @Description:邻接表表示的图结构头文件
 * @hank.jhk@outlook.com
 * @Copyright (c) 2022 by JHank, All Rights Reserved.
 */

#ifndef _ADJACENCYLIST_H
#define _ADJACENCYLIST_H

typedef int Vertex;         /*顶点编号,使用int*/
typedef int WeightType;     /*边权值类型为int*/
typedef char DataType;      /*顶点存储的数据类型*/

/*顶点数据数据结构*/
typedef struct
{
    int a;
}AdjDataType,*pAdjDataType;


/*邻接节点定义*/
typedef struct Adjvnode
{
    Vertex adjV;            /*邻接节点编号*/
    WeightType weight;      /*边权值*/
    struct Adjvnode *next;  /*邻接表链域,下一邻接节点*/
} EdgeNode,*pEdgeNode;


/*顶点节点定义*/
typedef struct Vertexnode
{
    Vertex adjV;                /*顶点节点编号*/
    int numVEdges;              /*顶点节点边数*/
    pAdjDataType pvertexData;   /*顶点数据指针*/
    pEdgeNode pfirstEdge;       /*邻接表链域,邻接表头指针*/
    struct Vertexnode *next;    /*标头链域,下一顶点节点*/
} VertexNode,*pVertexNode;


/********图-边链表操作函数*********/
pEdgeNode AdjGraphCreateENode(Vertex adjV,WeightType weight);
void AdjGraphInsertLastENode(pEdgeNode* ppheader, pEdgeNode pnode);
void AdjGraphInsertHeadENode(pEdgeNode* ppheader, pEdgeNode pnode);
bool AdjGraphDeleteHeadENode(pEdgeNode* ppheader);
bool AdjGraphDeleteLastENode(pEdgeNode* ppheader);
bool AdjGraphDeleteENode(pEdgeNode* ppheader, Vertex adjV);
void AdjGraphEmptyENode(pEdgeNode* ppheader);
bool AdjGraphFindENode(pEdgeNode* ppheader, Vertex adjV, WeightType* weightBack);
int AdjGraphELength(pEdgeNode* ppheader);

/*******图-图节点链表操作函数******/
void AdjGraphInit(pVertexNode pheader);
pVertexNode AdjGraphCreateVNode(Vertex adjV, pAdjDataType pvertexData,\
                             int numVEdges,Vertex adjVE[],WeightType weightE[]);
bool AdjGraphInsertVNode(pVertexNode* ppheader, pVertexNode pnode, int addr);
void AdjGraphInsertLastVNode(pVertexNode* ppheader, pVertexNode pnode);
void AdjGraphInsertHeadVNode(pVertexNode* ppheader, pVertexNode pnode);
bool AdjGraphDeleteHeadVNode(pVertexNode* ppheader);
bool AdjGraphDeleteLastVNode(pVertexNode* ppheader);
bool AdjGraphDeleteVNode(pVertexNode* ppheader, Vertex adjV);
void AdjGraphEmptyVNode(pVertexNode* ppheader);
bool AdjGraphFindVNode(pVertexNode* ppheader, Vertex adjV, pAdjDataType pvertexData);
int AdjGraphVLength(pVertexNode* ppheader);
pVertexNode AdjGraphVReverse(pVertexNode oldHeader);



#endif

